import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.message.DirectoryResult;
import org.xivo.cti.model.DirectoryEntry;
import org.xivo.cti.parser.DirectoryResultParser;


public class DirectoryResultParserTest {
    private DirectoryResultParser directoryResultParser;

    @Before
    public void setUp() throws Exception {
        directoryResultParser = new DirectoryResultParser();
    }

    @Test
    public void parseResult() throws JSONException {
        JSONObject jsonDirectoryResult  = new JSONObject(
                "{\"status\": \"ok\", " +
                "\"resultlist\": [\"Catherine BOURNE;2724;Inconnue;;R\u00e9pertoire XiVO Interne\", " +
                                    "\"Jeanne Gu\u00e9briant;;Inconnue;;R\u00e9pertoire XiVO Interne\", " +
                                    "\"Bill Johnson;;Inconnue;;R\u00e9pertoire XiVO Interne\", " +
                                    "\"St\u00e9phane ROBOULARD;1501;Inconnue;;R\u00e9pertoire XiVO Interne\", " +
                                    "\"Eric BUGARD;1503;Inconnue;;R\u00e9pertoire XiVO Interne\"], " +
                "\"timenow\": 1378813893.18, " +
                "\"headers\": [\"Nom\", \"Num\u00e9ro\", \"Entreprise\", \"E-mail\", \"Source\"], " +
                "\"replyid\": 4, " +
                "\"class\": \"directory\"}"
                );

        DirectoryResult result = directoryResultParser.parse(jsonDirectoryResult);
        assertNotNull("unable to parse directory result",result);
        assertThat(result.getHeaders(), hasItem("Entreprise"));

    }

    @Test
    public void parseResultCheckEntries() throws JSONException {
        JSONObject jsonDirectoryResult  = new JSONObject(
                "{\"status\": \"ok\", " +
                "\"resultlist\": [\"Catherine BOURNE;2724;Boogle;cbourne@boogle.fr\", " +
                                    "\"Eric BUGARD;1503;Inconnue;\"], " +
                "\"timenow\": 1378813893.18, " +
                "\"headers\": [\"Nom\", \"Num\u00e9ro\", \"Entreprise\", \"E-mail\"], " +
                "\"replyid\": 4, " +
                "\"class\": \"directory\"}"
                );

        DirectoryResult result = directoryResultParser.parse(jsonDirectoryResult);
        assertNotNull("unable to parse directory result",result);
        assertThat(result.getHeaders(), hasItem("Entreprise"));

        DirectoryEntry entry = new DirectoryEntry();
        entry.addField("Catherine BOURNE");
        entry.addField("2724");
        entry.addField("Boogle");
        entry.addField("cbourne@boogle.fr");

        assertThat(result.getEntries(), hasItem(entry));
        assertEquals("unable to get entries from result", 2,result.getEntries().size());

    }
}
