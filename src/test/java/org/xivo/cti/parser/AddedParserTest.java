package org.xivo.cti.parser;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.message.request.UsersAdded;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class AddedParserTest {
    AddedParser addedParser;

    @Before
    public void setUp() throws Exception {
        addedParser = new AddedParser();
    }


    // {"function": "addconfig", "listname": "users", "tipbxid": "xivo", "list": ["99"], "timenow": 1417188396.2, "class": "getlist"}

    @Test
    public void parseAddUser() throws JSONException {
        JSONObject userAddedMsg = new JSONObject("{\"function\": \"addconfig\", \"listname\": \"users\", " +
                "\"tipbxid\": \"xivo\", \"list\": [\"99\",\"54\"], \"timenow\": 1417188396.2, \"class\": \"getlist\"}");
        UsersAdded usersAdded = (UsersAdded) addedParser.parse(userAddedMsg);

        assertNotNull("Unable to parse user added", usersAdded);
        assertThat("list not decoded",usersAdded.getIds(),hasItem(Integer.valueOf(99)));

    }

}
