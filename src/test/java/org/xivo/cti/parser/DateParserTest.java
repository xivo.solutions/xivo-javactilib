package org.xivo.cti.parser;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;

public class DateParserTest {

    @Test
    public void testParse() {
        assertEquals("could not parse", new Date(1230456789123L), DateParser.parse(1230456789.123412));
    }
}
