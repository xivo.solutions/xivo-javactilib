package org.xivo.cti.parser;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.message.QueueStatistics;
import org.xivo.cti.model.Counter;
import org.xivo.cti.model.StatName;

public class StatParserTest {
    private StatParser statParser;

    @Before
    public void setUp() throws Exception {
        Logger.getLogger("").setLevel(Level.OFF);
        statParser = new StatParser();
    }

    @Test
    public void parseStatistic() throws JSONException {
        JSONObject jsonQueueStatistic = new JSONObject(
                "{\"stats\": {\"10\": {\"Xivo-LoggedAgents\": 12}},"
                        + "\"class\": \"getqueuesstats\", \"timenow\": 1384509582.88}");

        QueueStatistics queueStatistics = statParser.parse(jsonQueueStatistic);
        assertNotNull("unable to parse queue statistics", queueStatistics);
        assertEquals("invalid queue id", 10, queueStatistics.getQueueId());

        Counter statCounter = new Counter(StatName.LoggedAgents, 12);

        assertThat("unable to get number of logged agents", queueStatistics.getCounters(), hasItem(statCounter));
    }

    @Test
    public void parseUnknownCounter() throws JSONException {
        JSONObject jsonQueueStatistic = new JSONObject(
                "{\"stats\": {\"10\": {\"Xivo-Unknownn\": 12}},"
                        + "\"class\": \"getqueuesstats\", \"timenow\": 1384509582.88}");
        QueueStatistics queueStatistics = statParser.parse(jsonQueueStatistic);
        assertTrue("no counters in queue satistics", queueStatistics.getCounters().isEmpty());
    }

    @Test
    public void parseQueueuStatisticsAgents() throws JSONException {
        JSONObject jsonQueueStatistic = new JSONObject(
                "{\"stats\": {\"2\": {\"Xivo-TalkingAgents\": \"4\", \"Xivo-AvailableAgents\": \"7\", \"Xivo-EWT\": \"123\", \"Xivo-LongestWaitTime\": \"15\"}}, \"class\": \"getqueuesstats\", \"timenow\": 1384526058.77}");

        QueueStatistics queueStatistics = statParser.parse(jsonQueueStatistic);

        Counter talkingAgent = new Counter(StatName.TalkingAgents, 4);
        Counter availableAgents = new Counter(StatName.AvailableAgents, 7);
        Counter ewt = new Counter(StatName.EWT, 123);
        Counter longestWT = new Counter(StatName.LongestWaitTime, 15);

        assertThat("unable to get number of talking agents", queueStatistics.getCounters(), hasItem(talkingAgent));
        assertThat("unable to get number of available agents", queueStatistics.getCounters(), hasItem(availableAgents));
        assertThat("unable to get ewt", queueStatistics.getCounters(), hasItem(ewt));
        assertThat("unable to get ewt", queueStatistics.getCounters(), hasItem(longestWT));
    }

    @Test
    public void parseQueueuStatisticsOnRequests() throws JSONException {
        JSONObject jsonQueueStatistic = new JSONObject(
                "{\"stats\": {\"2\": {\"Xivo-Holdtime-avg\": \"34\", "
                        + "\"Xivo-Holdtime-max\": \"\", "
                        + "\"Xivo-Join\": \"4\", "
                        + "\"Xivo-Link\": \"56\", "
                        + "\"Xivo-Lost\": \"12\", "
                        + "\"Xivo-Qos\": \"\", "
                        + "\"Xivo-Rate\": \"\"}}, \"class\": \"getqueuesstats\", \"timenow\": 1384526058.77}");

        QueueStatistics queueStatistics = statParser.parse(jsonQueueStatistic);

        Counter holdTimeAvg = new Counter(StatName.Holdtimeavg, 34);

        assertThat("unable to get number of talking agents", queueStatistics.getCounters(), hasItem(holdTimeAvg));
    }
}
