package org.xivo.cti.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.xivo.cti.model.AgentStatus;
import org.xivo.cti.model.Availability;
import org.xivo.cti.model.StatusReason;

public class AgentStatusParserTest {

    @Test
    public void parseStatusTest() {
        String phonenumber = "2002";

        JSONObject jsonStatus = null;
        try {
            jsonStatus = new JSONObject("{\"availability\": \"available\", \"phonenumber\": \"" + phonenumber + "\", "
                    + "\"on_call_acd\": false, \"on_call_nonacd\": true, \"on_wrapup\": false}");
        } catch (JSONException e) {
            fail("Unexpected test condition");
            e.printStackTrace();
        }

        AgentStatus status = AgentStatusParser.parseStatus(jsonStatus);
        assertEquals("unable to parse availability", status.getAvailability(), Availability.AVAILABLE);
        assertEquals("unable to parse phonenumber", status.getPhonenumber(), phonenumber);
        assertEquals("unable to parse the reason", StatusReason.ON_CALL_NONACD, status.getReason());
    }

    @Test
    public void parseStatusWithMissingKeyTest() {
        String phonenumber = "2002";

        JSONObject jsonStatus = null;
        try {
            jsonStatus = new JSONObject("{\"availability\": \"available\", \"phonenumber\": \"" + phonenumber + "\", "
                    + "\"on_call_nonacd\": true, \"on_wrapup\": false}");
        } catch (JSONException e) {
            fail("Unexpected test condition");
            e.printStackTrace();
        }

        AgentStatusParser.parseStatus(jsonStatus);
    }

    @Test
    public void parseSinceAgentStatus() {
        JSONObject jsonStatus = null;
        try {
            jsonStatus = new JSONObject(
                    "{\"availability_since\": 1400541730.6, \"availability\": \"logged_out\", \"phonenumber\": null, "
                            + "\"on_call_acd\": false, \"on_call_nonacd\": false, \"on_wrapup\": false}");
        } catch (JSONException e) {
            fail("Unexpected test condition");
            e.printStackTrace();
        }
        long sincetime = 1400541730600L;

        AgentStatus status = AgentStatusParser.parseStatus(jsonStatus);
        assertEquals("unable to parse since", new Date(sincetime), status.getSince());
    }

    @Test
    public void parseAgentStatusQueues() throws JSONException {
        JSONObject jsonStatus = null;
        jsonStatus = new JSONObject(
                "{\"availability_since\": 1400541730.6, \"availability\": \"logged_out\", \"phonenumber\": null, "
                        + "\"on_call_acd\": false, \"on_call_nonacd\": false, \"on_wrapup\": false,\"queues\": [\"1\",\"11\",\"7\"]}");

        AgentStatus status = AgentStatusParser.parseStatus(jsonStatus);
        List<Integer> queues = new ArrayList<Integer>();
        queues.add(1);
        queues.add(11);
        queues.add(7);
        assertEquals("unable to parse the queues", queues, status.getQueues());
    }
}
