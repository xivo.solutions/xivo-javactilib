package org.xivo.cti.parser;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.message.AgentIds;
import org.xivo.cti.message.PhoneIdsList;
import org.xivo.cti.message.QueueIds;
import org.xivo.cti.message.QueueMemberIds;
import org.xivo.cti.message.UserIdsList;

public class ListParserTest {
    ListParser listParser;

    @Before
    public void setUp() throws Exception {
        listParser = new ListParser();
    }

    @Test
    public void parseUserIdsList() throws JSONException {
        JSONObject jsonUsersList = new JSONObject("{\"function\": \"listid\", " +
                                                    "\"listname\": \"users\", " +
                                                    "\"class\": \"getlist\"," +
                                                    "\"tipbxid\": \"xivo\", " +
                                                    "\"list\": [\"11\", \"25\", \"12\", \"14\"], " +
                                                    "\"timenow\": 1364919114.1 " +
                                                    "}");
        UserIdsList usersList = (UserIdsList) listParser.parse(jsonUsersList);
        assertNotNull("unable to decode users list",usersList);
        assertThat("list not decoded",usersList.getIds(),hasItem(Integer.valueOf(25)));

    }

    @Test
    public void parsePhoneIds() throws JSONException {
        JSONObject jsonPhonesList = new JSONObject("{\"function\": \"listid\", " +
                                                    "\"listname\": \"phones\", " +
                                                    "\"class\": \"getlist\"," +
                                                    "\"tipbxid\": \"xivo\", " +
                                                    "\"list\": [\"7\", \"23\", \"12\", \"14\"], " +
                                                    "\"timenow\": 1364919114.1 " +
                                                    "}");
        PhoneIdsList phonesList = (PhoneIdsList) listParser.parse(jsonPhonesList);
        assertNotNull("unable to decode phone ids",phonesList);
        assertThat("list not decoded",phonesList.getIds(),hasItem(Integer.valueOf(23)));

    }

    @Test
    public void parseQueuesIds() throws JSONException {
        JSONObject jsonQueueIds = new JSONObject("{\"function\": \"listid\", " +
                                                    "\"listname\": \"queues\", " +
                                                    "\"tipbxid\": \"xivo\", " +
                                                    "\"list\": [\"1\", \"10\", \"3\", \"2\", \"5\", \"4\", \"7\", \"6\", \"9\", \"8\"], " +
                                                    "\"timenow\": 1382693143.28, " +
                                                    "\"class\": \"getlist\"}");
        QueueIds queueIds = (QueueIds) listParser.parse(jsonQueueIds);
        assertNotNull("unable to decode queue ids",queueIds);
        assertThat("list not decoded",queueIds.getIds(),hasItem(Integer.valueOf(6)));
    }

    @Test
    public void parseAgentsIds() throws JSONException {
        JSONObject jsonAgentIds = new JSONObject("{\"function\": \"listid\", " +
                "\"listname\": \"agents\", " +
                "\"tipbxid\": \"xivo\", " +
                "\"list\": [\"1\", \"10\", \"3\", \"2\", \"5\", \"4\", \"7\", \"6\", \"9\", \"8\", \"17\"], " +
                "\"timenow\": 1382693143.28, " +
                "\"class\": \"getlist\"}");
        AgentIds agentIds = (AgentIds) listParser.parse(jsonAgentIds);
        assertNotNull("unable to decode agent ids",agentIds);
    }
    @Test
    public void parseQueueMembers() throws JSONException {
        JSONObject jsonQueueMemberIds = new JSONObject("{\"function\": \"listid\", " +
                                        "\"listname\": \"queuemembers\", " +
                                        "\"tipbxid\": \"xivo\", " +
                                        "\"list\": [\"Agent/2501,blue\", \"Agent/2500,yellow\", \"Agent/2002,yellow\", \"Agent/2003,__switchboard\", \"Agent/2003,blue\", \"Agent/108,blue\", \"Agent/2002,blue\", \"Agent/2500,red\", \"Agent/2500,green\", \"Agent/2500,blue\"], " +
                                        "\"timenow\": 1382715916.27, " +
                                        "\"class\": \"getlist\"}");
        QueueMemberIds queueMemberIds = (QueueMemberIds) listParser.parse(jsonQueueMemberIds);
        assertNotNull("unable to decode queue member ids",queueMemberIds);
        assertThat("list not decoded",queueMemberIds.getMemberIds(),hasItem("Agent/2003,blue"));
    }
}
