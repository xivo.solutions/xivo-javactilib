package org.xivo.cti.integration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.MessageDispatcher;
import org.xivo.cti.listener.ConfigListener;
import org.xivo.cti.listener.IdsListener;
import org.xivo.cti.listener.StatisticListener;
import org.xivo.cti.message.*;
import org.xivo.cti.model.QueueStatRequest;

public class ContactCenterItst extends CtiItst implements IdsListener, ConfigListener, StatisticListener {

    @Before
    public void setUp() throws Exception {
        messageDispatcher = new MessageDispatcher();
        messageDispatcher.addListener(LoginCapasAck.class, this);
        messageDispatcher.addListener(LoginIdAck.class, this);
        messageDispatcher.addListener(LoginPassAck.class, this);
        messageDispatcher.addListener(QueueIds.class, this);
        messageDispatcher.addListener(QueueConfigUpdate.class, this);
        messageDispatcher.addListener(QueueStatusUpdate.class, this);
        messageDispatcher.addListener(QueueMemberIds.class, this);
        messageDispatcher.addListener(QueueMemberConfigUpdate.class, this);
        messageDispatcher.addListener(QueueStatistics.class, this);
        messageDispatcher.addListener(AgentIds.class, this);
        messageDispatcher.addListener(AgentConfigUpdate.class, this);
        messageDispatcher.addListener(QueueMemberRemoved.class, this);
    }

    @Test
    public void requestContactCenterConfig() throws IOException, JSONException, TimeoutException {
        connectToServer();
        loginToCtiServer();

//        getQueues();
//
//        getAgents();
//
//        getQueueMembers();
//
        removeAgentFromQueue();

//        subscribeToStats();

        consumeMessages(300);

        disconnect();
    }

    private void addAgentToQueue() {
        JSONObject addAgent = messageFactory.createAddAgentToQueue("19","6");
        sendMessage(addAgent);
    }
    private void removeAgentFromQueue()  {
        JSONObject addAgent = messageFactory.createRemoveAgentFromQueue("19","6");
        sendMessage(addAgent);
    }
    private void getQueues() {
        JSONObject query = messageFactory.createGetQueues();
        sendMessage(query);
        consumeMessages(1);
    }

    private void getAgents() {
        JSONObject query = messageFactory.createGetAgents();
        sendMessage(query);
        consumeMessages(1);
    }

    private void getQueueMembers() {
        JSONObject query = messageFactory.createGetQueueMembers();
        sendMessage(query);
        consumeMessages(1);
    }

    private void subscribeToStats() {
        JSONObject query = messageFactory.createSubscribeToQueueStats();
        sendMessage(query);
    }

    @Override
    public void onUserIdsLoaded(UserIdsList userIdsList) {
    }

    @Override
    public void onPhoneIdsLoaded(PhoneIdsList phoneIdsList) {
    }

    @Override
    public void onQueueIdsLoaded(QueueIds queueIds) {
        System.out.println("queues id loaded " + queueIds.toString());
        for (Integer queueId : queueIds.getIds()) {
            System.out.println("Requesting config for queue id : " + queueId);
            JSONObject getQueueConfigMessage = messageFactory.createGetQueueConfig(queueId.toString());
            sendMessage(getQueueConfigMessage);
        }
        consumeMessages(queueIds.getIds().size());
    }

    @Override
    public void onQueueConfigUpdate(QueueConfigUpdate queueConfigUpdate) {
        System.out.println("Queue config update : " + queueConfigUpdate.toString());
        JSONObject getQueueStatusMessage = messageFactory.createGetQueueStatus(queueConfigUpdate.getId().toString());
        sendMessage(getQueueStatusMessage);
        consumeMessages(1);
        List<QueueStatRequest> requests = new ArrayList<QueueStatRequest>();
        requests.add(new QueueStatRequest(queueConfigUpdate.getId().toString(), 3600, 60));
        JSONObject getStatistics = messageFactory.createGetQueueStatistics(requests);
        sendMessage(getStatistics);
        consumeMessages(1);
    }

    @Override
    public void onQueueStatusUpdate(QueueStatusUpdate queueStatusUpdate) {
        System.out.println("Queue status update : " + queueStatusUpdate.toString());
    }

    @Override
    public void onQueueMemberIdsLoaded(QueueMemberIds queueMemberIds) {
        System.out.println("Queue member ids " + queueMemberIds.toString());
        for (String memberId : queueMemberIds.getMemberIds()) {
            System.out.println("Requesting config for member id : " + memberId);
            JSONObject getQueueConfigMessage = messageFactory.createGetQueueMemberConfig(memberId);
            sendMessage(getQueueConfigMessage);
        }
        consumeMessages(queueMemberIds.getMemberIds().size());
    }

    @Override
    public void onAgentIdsLoaded(AgentIds agentIds) {
        System.out.println("Agent id loaded " + agentIds.toString());
        for (Integer agentId : agentIds.getIds()) {
            System.out.println("Requesting status for agent id : " + agentId);
            JSONObject getAgentConfigMessage = messageFactory.createGetAgentConfig(agentId.toString());
            sendMessage(getAgentConfigMessage);
        }
        consumeMessages(agentIds.getIds().size());
    }

    @Override
    public void onUserConfigUpdate(UserConfigUpdate userConfigUpdate) {
    }

    @Override
    public void onUserStatusUpdate(UserStatusUpdate userStatusUpdate) {
    }

    @Override
    public void onPhoneConfigUpdate(PhoneConfigUpdate phoneConfigUpdate) {
    }

    @Override
    public void onPhoneStatusUpdate(PhoneStatusUpdate phoneStatusUpdate) {
    }

    @Override
    public void onQueueMemberConfigUpdate(QueueMemberConfigUpdate queueMemberConfigUpdate) {
        System.out.println(queueMemberConfigUpdate);
    }

    @Override
    public void onstatisticsUpdated(QueueStatistics queueStatistics) {
        System.out.println(queueStatistics);

    }

    @Override
    public void onAgentConfigUpdate(AgentConfigUpdate agentConfigUpdate) {
        System.out.println(agentConfigUpdate);
    }

    @Override
    public void onQueueMemberRemoved(QueueMemberRemoved queueMemberRemoved) {
        System.out.println("Queue memeber removed :" + queueMemberRemoved);

    }

    @Override
    public void onMeetmeUpdate(MeetmeUpdate meetmeUpdate) {

    }

    @Override
    public void onVoiceMailStatusUpdate(VoiceMailStatusUpdate voiceMailStatusUpdate) {

    }

}
