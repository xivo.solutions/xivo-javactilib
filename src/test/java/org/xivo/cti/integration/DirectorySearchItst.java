package org.xivo.cti.integration;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.MessageDispatcher;
import org.xivo.cti.listener.DirectoryListener;
import org.xivo.cti.listener.LoginStepListener;
import org.xivo.cti.message.DirectoryResult;
import org.xivo.cti.message.LoginCapasAck;
import org.xivo.cti.message.LoginIdAck;
import org.xivo.cti.message.LoginPassAck;
import org.xivo.cti.model.DirectoryEntry;

public class DirectorySearchItst extends CtiItst implements LoginStepListener, DirectoryListener{


    @Before
    public void setUp() throws Exception {
        messageDispatcher = new MessageDispatcher();
        messageDispatcher.addListener(LoginCapasAck.class,this);
        messageDispatcher.addListener(LoginIdAck.class,this);
        messageDispatcher.addListener(LoginPassAck.class, this);
        messageDispatcher.addListener(DirectoryResult.class, this);
    }

    @Test
    public void requestSearchDirectory() throws IOException, JSONException, TimeoutException {
        connectToServer();
        loginToCtiServer();

        searchDirectory("b");

        DirectoryResult directoryResult = (DirectoryResult) waitForMessage(DirectoryResult.class.toString());
        assertNotNull("did not get any result from directory request",directoryResult);
        messageDispatcher.dispatch(directoryResult);

        disconnect();
    }

    private void searchDirectory(String pattern) {
        JSONObject message = messageFactory.createSearchDirectory(pattern);
        sendMessage(message);
    }


    @Override
    public void onDirectoryResult(DirectoryResult directoryResult) {
        System.out.println("<-----------Directory result ---------->");
        for(String header : directoryResult.getHeaders()) {
            System.out.print(header + " - " );
        }
        System.out.println("");
        int i = 0;
        for (DirectoryEntry entry : directoryResult.getEntries()) {
            System.out.println("["+i++ + "] : " + entry);
        }
    }
}
