package org.xivo.cti.integration;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.xivo.cti.MessageDispatcher;
import org.xivo.cti.listener.ConfigListener;
import org.xivo.cti.message.*;

public class ServiceItst extends CtiItst implements ConfigListener {

    private boolean dndState = false;
    private boolean uncFwdSate = false;
    private String uncFwdDest = "";
    private boolean naFwdSate = false;
    private String naFwdDest = "";
    private boolean busyFwdSate = false;
    private String busyFwdDest = "";

    @Before
    public void setUp() throws Exception {
        messageDispatcher = new MessageDispatcher();
        messageDispatcher.addListener(LoginCapasAck.class, this);
        messageDispatcher.addListener(LoginIdAck.class, this);
        messageDispatcher.addListener(LoginPassAck.class, this);
        messageDispatcher.addListener(UserStatusUpdate.class, this);
        messageDispatcher.addListener(UserConfigUpdate.class, this);
    }

    @Test
    public void requestContactCenterConfig() throws IOException, JSONException, TimeoutException {
        connectToServer();
        loginToCtiServer();

        getUserConfig();
        toggleDnd();
        enableUnc();
        enableNaFwd();
        enableBusyFwd();
        getUserConfig();
        consumeMessages(10);

        disconnect();
    }

    private void toggleDnd() {
        sendMessage(messageFactory.createDND(!dndState));
        consumeMessages(1);
    }

    private void enableUnc() {
        sendMessage(messageFactory.createUnconditionnalForward("12345", false));
        consumeMessages(1);
    }

    private void enableNaFwd() {
        sendMessage(messageFactory.createNaForward("78956", false));
        consumeMessages(1);
    }

    private void enableBusyFwd() {
        sendMessage(messageFactory.createBusyForward("45897", false));
        consumeMessages(1);
    }

    private void getUserConfig() {
        sendMessage(messageFactory.createGetUserConfig(userId));
        consumeMessages(1);
    }

    @Override
    public void onUserConfigUpdate(UserConfigUpdate userConfigUpdate) {
        if (String.valueOf(userConfigUpdate.getUserId()).equals(userId)) {
            System.out.println("This is my configuration " + userConfigUpdate.getUserId());
            dndState = userConfigUpdate.isDndEnabled();
            if (userConfigUpdate.getUncFwdDestination() != null) {
                uncFwdSate = userConfigUpdate.isUncFwdEnabled();
                uncFwdDest = userConfigUpdate.getUncFwdDestination();
            }
            if (userConfigUpdate.getNaFwdDestination() != null) {
                naFwdSate = userConfigUpdate.isNaFwdEnabled();
                naFwdDest = userConfigUpdate.getNaFwdDestination();
            }
            if (userConfigUpdate.getBusyFwdDestination() != null) {
                busyFwdSate = userConfigUpdate.isBusyFwdEnabled();
                busyFwdDest = userConfigUpdate.getBusyFwdDestination();
            }
            System.out.println("Dnd enabled is : " + dndState);
            System.out.println("Unc Fwd enabled is : " + uncFwdSate + " dest : " + uncFwdDest);
            System.out.println("Na Fwd enabled is : " + naFwdSate + " dest : " + naFwdDest);
            System.out.println("Busy Fwd enabled is : " + busyFwdSate + " dest : " + busyFwdDest);
        } else {
            System.out.println("User config updated " + userConfigUpdate.getUserId());
        }
    }

    @Override
    public void onUserStatusUpdate(UserStatusUpdate userStatusUpdate) {
        System.out.println("User " + userStatusUpdate.getUserId() + " status update " + userStatusUpdate.getStatus());

    }

    @Override
    public void onPhoneConfigUpdate(PhoneConfigUpdate phoneConfigUpdate) {
    }

    @Override
    public void onPhoneStatusUpdate(PhoneStatusUpdate phoneStatusUpdate) {
    }

    @Override
    public void onQueueConfigUpdate(QueueConfigUpdate queueConfigUpdate) {
    }

    @Override
    public void onQueueStatusUpdate(QueueStatusUpdate queueStatusUpdate) {
    }

    @Override
    public void onQueueMemberConfigUpdate(QueueMemberConfigUpdate queueMemberConfigUpdate) {
    }

    @Override
    public void onAgentConfigUpdate(AgentConfigUpdate agentConfigUpdate) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onQueueMemberRemoved(QueueMemberRemoved queueMemberRemoved) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onMeetmeUpdate(MeetmeUpdate meetmeUpdate) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onVoiceMailStatusUpdate(VoiceMailStatusUpdate voiceMailStatusUpdate) {

    }

}
