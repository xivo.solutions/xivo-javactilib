package org.xivo.cti;

import org.apache.commons.lang3.builder.ToStringBuilder;


public class CtiMessage {
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
