package org.xivo.cti.listener;

import org.xivo.cti.message.AgentIds;
import org.xivo.cti.message.PhoneIdsList;
import org.xivo.cti.message.QueueIds;
import org.xivo.cti.message.QueueMemberIds;
import org.xivo.cti.message.UserIdsList;

public interface IdsListener {

    void onUserIdsLoaded(UserIdsList userIdsList);

    void onPhoneIdsLoaded(PhoneIdsList phoneIdsList);

    void onQueueIdsLoaded(QueueIds queueIds);

    void onQueueMemberIdsLoaded(QueueMemberIds queueMemberIds);

    void onAgentIdsLoaded(AgentIds agentIds);

}
