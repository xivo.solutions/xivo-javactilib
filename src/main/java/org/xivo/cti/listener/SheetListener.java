package org.xivo.cti.listener;

import org.xivo.cti.message.Sheet;

public interface SheetListener {
    void onSheetReceived(Sheet sheet);
}
