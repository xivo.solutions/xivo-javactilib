package org.xivo.cti.listener;

import org.xivo.cti.message.DirectoryResult;


public interface DirectoryListener {

    void onDirectoryResult(DirectoryResult directoryResult);

}
