package org.xivo.cti.listener;

import org.xivo.cti.message.QueueStatistics;

public interface StatisticListener {

    void onstatisticsUpdated(QueueStatistics queueStatistics);

}
