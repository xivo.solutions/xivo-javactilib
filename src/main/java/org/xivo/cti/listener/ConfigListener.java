package org.xivo.cti.listener;

import org.xivo.cti.message.*;

public interface ConfigListener {

    void onUserConfigUpdate(UserConfigUpdate userConfigUpdate);

    void onUserStatusUpdate(UserStatusUpdate userStatusUpdate);

    void onPhoneConfigUpdate(PhoneConfigUpdate phoneConfigUpdate);

    void onPhoneStatusUpdate(PhoneStatusUpdate phoneStatusUpdate);

    void onQueueConfigUpdate(QueueConfigUpdate queueConfigUpdate);

    void onQueueStatusUpdate(QueueStatusUpdate queueStatusUpdate);

    void onAgentConfigUpdate(AgentConfigUpdate agentConfigUpdate);

    void onQueueMemberConfigUpdate(QueueMemberConfigUpdate queueMemberConfigUpdate);

    void onQueueMemberRemoved(QueueMemberRemoved queueMemberRemoved);

    void onMeetmeUpdate(MeetmeUpdate meetmeUpdate);

    void onVoiceMailStatusUpdate(VoiceMailStatusUpdate voiceMailStatusUpdate);
}
