package org.xivo.cti.listener;

import org.xivo.cti.message.IpbxCommandResponse;

public interface IpbxCommandResponseListener {

    public void onIpbxCommandResponse(IpbxCommandResponse ipbxCommandResponse);

}
