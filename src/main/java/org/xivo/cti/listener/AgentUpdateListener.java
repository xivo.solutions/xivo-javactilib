package org.xivo.cti.listener;

import org.xivo.cti.message.PhoneConfigUpdate;
import org.xivo.cti.message.PhoneStatusUpdate;
import org.xivo.cti.message.AgentStatusUpdate;

public interface AgentUpdateListener {
	// TODO to be implemented
    // public void onAgentConfigUpdate(AgentConfigUpdate AgentConfigUpdate);

    public void onAgentStatusUpdate(AgentStatusUpdate AgentStatusUpdate);

    public void onPhoneConfigUpdate(PhoneConfigUpdate phoneConfigUpdate);

    public void onAgentStatusUpdate(PhoneStatusUpdate phoneStatusUpdate);
}
