package org.xivo.cti.listener;

import org.xivo.cti.message.ChannelStatus;

public interface ChannelUpdateListener {

    void onChannelStatusUpdate(ChannelStatus channelStatusUpdate);

}
