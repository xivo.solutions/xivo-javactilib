package org.xivo.cti.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class AgentStatus {
    private final String phonenumber;
    private final Availability availability;
    private StatusReason reason;
    private Date since;
    List<Integer> queues = new ArrayList<Integer>();

    public AgentStatus(String phonenumber, Availability availability, StatusReason reason) {
        this.phonenumber = phonenumber;
        this.availability = availability;
        this.reason = reason;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public Availability getAvailability() {
        return availability;
    }

    public StatusReason getReason() {
        return reason;
    }

    public Date getSince() {
        return since;
    }

    public void setSince(Date since) {
        this.since = since;
    }

    public List<Integer> getQueues() {
        return queues;
    }

    public void setQueues(List<Integer> queues) {
        this.queues = queues;
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this , obj, new ArrayList<String>().add("since"));
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
