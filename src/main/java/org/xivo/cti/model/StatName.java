package org.xivo.cti.model;

public enum StatName {
    LoggedAgents,
    TalkingAgents,
    AvailableAgents,
    EWT,
    Holdtimeavg,
    Holdtimemax,
    Join,
    Link,
    Lost,
    Qos,
    Rate,
    WaitingCalls,
    LongestWaitTime
}
