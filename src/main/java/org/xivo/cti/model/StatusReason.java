package org.xivo.cti.model;

public enum StatusReason {
    ON_CALL_ACD, ON_CALL_NONACD, ON_WRAPUP, NONE
}
