package org.xivo.cti.model;

import java.util.Date;

public class MeetmeMember {

    private int joinOrder;
    private Date joinTime;
    private boolean muted;
    private String name;
    private String number;

    public MeetmeMember(int joinOrder, Date joinTime, boolean muted, String name, String number) {
        this.joinOrder = joinOrder;
        this.joinTime = joinTime;
        this.muted = muted;
        this.name = name;
        this.number = number;
    }

    public int getJoinOrder() {
        return joinOrder;
    }

    public Date getJoinTime() {
        return joinTime;
    }

    public boolean isMuted() {
        return muted;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

}
