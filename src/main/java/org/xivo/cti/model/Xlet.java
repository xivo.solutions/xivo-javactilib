package org.xivo.cti.model;

public class Xlet {
	private final String name;
	private final String container;
	private final String order;

	public Xlet(String name, String container, String order) {
		this.name = name;
		this.container = container;
		this.order = order;
	}
	
	public String getName() {
		return name;
	}

	public String getContainer() {
		return container;
	}

	public String getOrder() {
		return order;
	}

	@Override
	public boolean equals(Object obj) {
		Xlet xlet = (Xlet) obj;
		return xlet.name.equals(name) 
				&& xlet.container.equals(container) && xlet.order.equals(order);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return name + " " + container + " " + order;
	}
}
