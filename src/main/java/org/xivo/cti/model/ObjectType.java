package org.xivo.cti.model;

public enum ObjectType {
    PHONES, USERS, AGENTS, CHANNELS, QUEUES, QUEUEMEMBERS, VOICEMAILS
}
