package org.xivo.cti.model;

public enum CallType {
    INBOUND, OUTBOUND, MISSED
}
