package org.xivo.cti.model.helpers;

import org.xivo.cti.model.Endpoint;

public class EndpointFactory {
    public Endpoint getEndpoint(Endpoint.EndpointType type, String value) {
        return new Endpoint(type, value);
    }

}
