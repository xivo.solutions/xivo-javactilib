package org.xivo.cti.model;

public class Endpoint {
    private String value = "";

    private final String XIVO_ID_PART = ":xivo/";

    public enum EndpointType {
        INVALID ("invalid_destination_url"),
        EXTEN ("exten"),
        PHONE ("phone");

        private final String name;

        private EndpointType(String s) {
            name = s;
        }

        public boolean equalsName(String otherName){
            return (otherName == null)? false:name.equals(otherName);
        }

        public String toString(){
            return name;
        }
    }

    private EndpointType type = EndpointType.INVALID;

    public Endpoint(EndpointType type, String id) {
        this.value = id;
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public EndpointType getType() {
        return type;
    }

    public String getDestinationUrl() {
        return type.name + XIVO_ID_PART + value;
    }

    @Override
    public String toString() {
        return getDestinationUrl();
    }
}
