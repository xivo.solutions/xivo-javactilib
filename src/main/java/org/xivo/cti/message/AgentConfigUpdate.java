package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

public class AgentConfigUpdate  extends ConfigUpdate {
    private String lastName;
    private String firstName;
    private String number;

    @Override
    public void notify(ConfigListener listener) {
        listener.onAgentConfigUpdate(this);
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }
}
