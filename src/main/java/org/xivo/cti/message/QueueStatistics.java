package org.xivo.cti.message;

import java.util.ArrayList;
import java.util.List;

import org.xivo.cti.listener.StatisticListener;
import org.xivo.cti.model.Counter;

public class QueueStatistics extends CtiResponseMessage<StatisticListener> {
    private int queueId;
    private List<Counter> counters = new ArrayList<Counter>();

    @Override
    public void notify(StatisticListener listener) {
        listener.onstatisticsUpdated(this);
    }

    public int getQueueId() {
        return queueId;
    }

    public void setQueueId(int queueId) {
        this.queueId = queueId;
    }

    public List<Counter> getCounters() {
        return counters;
    }

    public void addCounter(Counter counter) {
        counters.add(counter);
    }

}
