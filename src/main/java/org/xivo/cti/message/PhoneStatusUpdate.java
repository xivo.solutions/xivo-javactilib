package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

public class PhoneStatusUpdate extends CtiResponseMessage<ConfigListener>{
    private Integer lineId;
    private String hintStatus;

    public Integer getLineId() {
        return lineId;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }

    public String getHintStatus() {
        return hintStatus;
    }

    public void setHintStatus(String hintStatus) {
        this.hintStatus = hintStatus;
    }

    @Override
    public void notify(ConfigListener listener) {
        listener.onPhoneStatusUpdate(this);

    }

}
