package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

public class PhoneConfigUpdate  extends ConfigUpdate {
    private Integer userId;
    private String number;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public void notify(ConfigListener listener) {
        listener.onPhoneConfigUpdate(this);

    }

}
