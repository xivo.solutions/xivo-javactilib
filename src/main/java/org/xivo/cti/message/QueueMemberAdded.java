package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

public class QueueMemberAdded extends CtiResponseMessage<ConfigListener> {
    private String agentNumber;
    private String queueName;

    @Override
    public void notify(ConfigListener listener) {
    }

    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

}
