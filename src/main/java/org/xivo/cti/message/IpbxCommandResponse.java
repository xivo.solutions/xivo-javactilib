package org.xivo.cti.message;

import org.xivo.cti.listener.IpbxCommandResponseListener;

import java.util.Date;

public class IpbxCommandResponse extends CtiResponseMessage<IpbxCommandResponseListener>{
    private String error_string;
    private Date timenow;

    public IpbxCommandResponse(String error_string, Date timenow) {
        this.error_string = error_string;
        this.timenow = timenow;
    }

    public String getError_string() {
        return error_string;
    }

    public Date getTimenow() {
        return timenow;
    }

    @Override
    public void notify(IpbxCommandResponseListener listener) {
        listener.onIpbxCommandResponse(this);
    }
}
