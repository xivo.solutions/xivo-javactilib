package org.xivo.cti.message;

import java.util.ArrayList;
import java.util.List;

import org.xivo.cti.listener.ConfigListener;

public class QueueStatusUpdate extends CtiResponseMessage<ConfigListener> {
    private final List<Integer> agentMembers;
    private final List<Integer> phoneMembers;

    public QueueStatusUpdate() {
        agentMembers = new ArrayList<Integer>();
        phoneMembers = new ArrayList<Integer>();
    }

    @Override
    public void notify(ConfigListener listener) {
        listener.onQueueStatusUpdate(this);
    }

    public List<Integer> getAgentMembers() {
        return agentMembers;
    }

    public void addAgentMember(Integer agentId) {
        agentMembers.add(agentId);
    }

    public List<Integer> getPhoneMembers() {
        return phoneMembers;
    }

    public void addPhoneMember(Integer phoneId) {
        phoneMembers.add(phoneId);
    }

}
