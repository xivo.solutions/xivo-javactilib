package org.xivo.cti.message;

import org.xivo.cti.listener.ChannelUpdateListener;


public class ChannelStatus extends CtiResponseMessage<ChannelUpdateListener> {

    public double timeStamp = 0.0f;
    public boolean holded = false;
    public String commstatus;
    public boolean parked = false;
    public String state;
    public String channelId;
    public String direction;
    public String talkingto_kind;
    public String talkingto_id;

    @Override
    public void notify(ChannelUpdateListener listener) {
        listener.onChannelStatusUpdate(this);

    }


}
