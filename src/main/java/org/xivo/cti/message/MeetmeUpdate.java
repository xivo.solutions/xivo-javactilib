package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;
import org.xivo.cti.model.Meetme;

import java.util.List;

public class MeetmeUpdate extends ConfigUpdate {

    private List<Meetme> meetmes;

    public MeetmeUpdate(List<Meetme> meetmes) {
        this.meetmes = meetmes;
    }

    @Override
    public void notify(ConfigListener listener) {
        listener.onMeetmeUpdate(this);
    }

    public List<Meetme> getMeetmeList() {
        return meetmes;
    }
}
