package org.xivo.cti.message.request;

public class CompleteTransfer extends CtiRequest {

    public CompleteTransfer() {
        super("complete_transfer");
    }

}
