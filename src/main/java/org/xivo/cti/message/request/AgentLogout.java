package org.xivo.cti.message.request;

public class AgentLogout extends IpbxCommand {
    private final String agentids;

    private final String AGENT_PREFIX = XIVO+"/";

    public AgentLogout(String agentId) {
        super("agentlogout");
        agentids = agentId;
    }

    public String getAgentIds() {
        return AGENT_PREFIX + agentids;
    }
}
