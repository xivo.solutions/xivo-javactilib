package org.xivo.cti.message.request;

public class InviteConferenceRoom extends CtiRequest {

    private int userId;

    public InviteConferenceRoom(int userId) {
        super("invite_confroom");
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }
}
