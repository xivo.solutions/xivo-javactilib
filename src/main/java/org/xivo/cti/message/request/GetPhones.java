package org.xivo.cti.message.request;

import org.xivo.cti.model.ObjectType;

public class GetPhones extends GetConfig {
    private static final String LISTID = "listid";

    public GetPhones() {
        super(LISTID,ObjectType.PHONES.toString());
    }

}
