package org.xivo.cti.message.request;

public class CancelTransfer extends CtiRequest {

    public CancelTransfer() {
        super("cancel_transfer");
    }

}
