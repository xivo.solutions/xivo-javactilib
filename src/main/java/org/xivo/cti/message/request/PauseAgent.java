package org.xivo.cti.message.request;

public class PauseAgent extends PauseUnPauseAgent {

    public PauseAgent(String agentId) {
        super("queuepause",agentId);
    }

}
