package org.xivo.cti.message.request;

public class SearchDirectory extends CtiRequest {
    private String pattern = "";

    public SearchDirectory(String pattern) {
        super("directory");
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }

}
