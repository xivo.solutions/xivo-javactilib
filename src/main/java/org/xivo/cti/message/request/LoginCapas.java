package org.xivo.cti.message.request;

public class LoginCapas extends CtiRequest {
	private final String claz =  "login_capas";
	private final String loginkind;
	private final int capaid;
	private final boolean lastconnwins = false;
	private final String state = "available";
	private final String agentNumber;

	public LoginCapas(int capaId) {
	    super("login_capas");
		this.loginkind = "user";
		this.capaid = capaId;
		this.agentNumber = null;
	}
	
	public LoginCapas(int capaId, String agentNumber) {
        super("login_capas");
		this.capaid = capaId;		
		this.loginkind = "agent";
		this.agentNumber = agentNumber;
	}

	public String getClaz() {
		return claz;
	}

	public String getLoginkind() {
		return loginkind;
	}
	
	public void setAgentNumber(String number) {
	}
	
	public String getAgentNumber() {
		return agentNumber; 
	}

	public int getCapaid() {
		return capaid;
	}

	public boolean isLastconnwins() {
		return lastconnwins;
	}

	public String getState() {
		return state;
	}

}
