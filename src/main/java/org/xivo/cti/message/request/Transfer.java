package org.xivo.cti.message.request;

public class Transfer extends CtiRequest{
    private String destination;

    public Transfer(String claz, String destination) {
        super(claz);
        this.destination = destination;
    }

    public String getDestination() {
        return destination;
    }

}
