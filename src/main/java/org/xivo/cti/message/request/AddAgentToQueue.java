package org.xivo.cti.message.request;

/**
 * Created by jylebleu on 25/08/14.
 */
public class AddAgentToQueue extends AgentQueueConfig {

    public AddAgentToQueue(String agentId, String queueId) {
        super(QUEUEADD, agentId,queueId);
    }
}
