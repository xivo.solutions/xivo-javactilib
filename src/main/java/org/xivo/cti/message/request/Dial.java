package org.xivo.cti.message.request;

import org.xivo.cti.model.Endpoint;
import org.xivo.cti.model.helpers.EndpointFactory;

public class Dial extends IpbxCommand {

    public Endpoint destination = new EndpointFactory().getEndpoint(Endpoint.EndpointType.INVALID, "");

    public Dial(Endpoint destination) {
        super("dial");
        this.destination = destination;
    }

    public Endpoint getDestination() {
        return destination;
    }

    public void setDestination(Endpoint destination) {
        this.destination = destination;
    }

}
