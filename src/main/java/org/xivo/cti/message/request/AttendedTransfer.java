package org.xivo.cti.message.request;

public class AttendedTransfer extends Transfer {

    public AttendedTransfer(String destination) {
        super("attended_transfer",destination);
    }
}
