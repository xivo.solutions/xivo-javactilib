package org.xivo.cti.message.request;

public class LoginId extends CtiRequest {
    private final String company = "default";
    private final String ident;
    private final String userlogin;
    private final String version = "9999";
    private String xivoversion;

    public LoginId(String username, String identity, String protocolVersion) {
        super("login_id");
        userlogin = username;
        ident = identity;
        xivoversion = protocolVersion;
    }

    public String getCompany() {
        return company;
    }

    public String getIdent() {
        return ident;
    }

    public String getUserlogin() {
        return userlogin;
    }

    public String getVersion() {
        return version;
    }

    public String getXivoversion() {
        return xivoversion;
    }

}
