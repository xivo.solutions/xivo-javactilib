package org.xivo.cti.message.request;

import org.xivo.cti.model.ObjectType;

public class GetQueueMembers extends GetConfig  {
    private static final String LISTID = "listid";

    public GetQueueMembers() {
        super(LISTID,ObjectType.QUEUEMEMBERS.toString());
    }

}
