package org.xivo.cti.message.request;


import org.xivo.cti.model.Endpoint;
import org.xivo.cti.model.helpers.EndpointFactory;

public class Originate extends IpbxCommand {

    private EndpointFactory endpointFactory = new EndpointFactory();
    private Endpoint source = endpointFactory.getEndpoint(Endpoint.EndpointType.INVALID, "");
    private Endpoint destination = endpointFactory.getEndpoint(Endpoint.EndpointType.INVALID, "");

    public Originate(Endpoint source, Endpoint destination) {
        super("originate");
        this.source = source;
        this.destination =  destination;
    }

    public Endpoint getSource() {
        return source;
    }

    public Endpoint getDestination() {
        return destination;
    }

}
