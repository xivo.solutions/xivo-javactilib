package org.xivo.cti.message.request;

public class UnconditionnalFwd extends Fwd {

    public UnconditionnalFwd(String destination, boolean enabled) {
        super(destination,enabled,fwdType.unc);
    }
}
