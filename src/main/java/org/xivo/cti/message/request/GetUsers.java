package org.xivo.cti.message.request;

import org.xivo.cti.model.ObjectType;


public class GetUsers extends GetConfig {
    private static final String LISTID = "listid";

    public GetUsers() {
        super(LISTID,ObjectType.USERS.toString());
    }

}
