package org.xivo.cti.message.request;

public class IpbxCommand extends CtiRequest {
    private String command;

    public IpbxCommand(String command) {
        super("ipbxcommand");
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

}
