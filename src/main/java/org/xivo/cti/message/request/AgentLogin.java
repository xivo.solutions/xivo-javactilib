package org.xivo.cti.message.request;

public class AgentLogin extends IpbxCommand {
    private final String phoneNumber;
    private final String agentids;
    private final String AGENT_PREFIX = XIVO + "/";

    public AgentLogin(String phoneNumber) {
        super("agentlogin");
        this.phoneNumber = phoneNumber;
        agentids = "agent:special:me";
    }

    public AgentLogin(String agentId, String phoneNumber) {
        super("agentlogin");
        this.phoneNumber = phoneNumber;
        agentids = AGENT_PREFIX + agentId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getAgentIds() {
        return agentids;
    }

}
