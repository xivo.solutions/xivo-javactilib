package org.xivo.cti.message.request;

public class GetConfig extends CtiRequest {
    private static final String XIVO = "xivo";
    private static final String GETLIST = "getlist";

    private final String tipBxid = XIVO;
    private final String listName;
    private final String function;


    public GetConfig(String function, String listName) {
        super(GETLIST);
        this.function = function;
        this.listName = listName.toLowerCase();
    }

    public String getTipBxid() {
        return tipBxid;
    }

    public String getListName() {
        return listName;
    }

    public String getFunction() {
        return function;
    }

}
