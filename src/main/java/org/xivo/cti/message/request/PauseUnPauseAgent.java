package org.xivo.cti.message.request;

public class PauseUnPauseAgent extends IpbxCommand {
    private String member = "agent:"+XIVO+"/";
    private final String queue = "queue:"+XIVO+"/all";

    public PauseUnPauseAgent(String command, String agentId) {
        super(command);
        member = member+agentId;
    }

    public String getMember() {
        return member;
    }

    public String getQueue() {
        return queue;
    }

}
