package org.xivo.cti.message.request;

import org.xivo.cti.CtiMessage;

public class CtiRequest extends CtiMessage {
    protected static final String XIVO = "xivo";
    private String claz = "";

    public CtiRequest(String claz) {
        this.claz = claz;
    }

    public String getClaz() {
        return claz;
    }

}
