package org.xivo.cti.message.request;

public class Fwd extends CtiRequest {
    private String destination;
    private boolean enabled;
    private fwdType type;

    public enum fwdType {
        rna, unc, busy
    }
    
    public Fwd(String destination, boolean enabled,fwdType type) {
        super("featuresput");
        this.destination = destination;
        this.enabled = enabled;
        this.type = type;
    }

    public String getFunction() {
        return "fwd";
    }

    public String getDestination() {
        return destination;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public String getType() {
        return type.name();
    }

}
