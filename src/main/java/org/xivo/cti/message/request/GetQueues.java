package org.xivo.cti.message.request;

import org.xivo.cti.model.ObjectType;

public class GetQueues extends GetConfig {
    private static final String LISTID = "listid";

    public GetQueues() {
        super(LISTID,ObjectType.QUEUES.toString());
    }

}
