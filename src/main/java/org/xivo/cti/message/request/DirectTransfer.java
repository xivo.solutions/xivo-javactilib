package org.xivo.cti.message.request;

public class DirectTransfer extends Transfer {

    public DirectTransfer(String destination) {
        super("direct_transfer",destination);
    }

}
