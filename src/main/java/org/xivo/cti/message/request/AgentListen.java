package org.xivo.cti.message.request;

public class AgentListen extends IpbxCommand {
    private final String agentids;

    private final String AGENT_PREFIX = XIVO+"/";

    public AgentListen(String agentId) {
        super("listen");
        agentids = agentId;
    }

    public String getAgentIds() {
        return AGENT_PREFIX + agentids;
    }

}
