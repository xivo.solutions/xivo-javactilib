package org.xivo.cti.message;

import org.xivo.cti.listener.IdsListener;

public class AgentIds extends IdsList {

    @Override
    public void notify(IdsListener listener) {
        listener.onAgentIdsLoaded(this);
    }

}
