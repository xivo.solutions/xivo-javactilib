package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

public abstract class ConfigUpdate extends CtiResponseMessage<ConfigListener> {
    private Integer id;
    private String context = "";

    public void setId(Integer id) {
        this.id = id;
    }
    public Integer getId() {
        return id;
    }
    public String getContext() {
        return context;
    }
    public void setContext(String context) {
        this.context = context;
    }
}
