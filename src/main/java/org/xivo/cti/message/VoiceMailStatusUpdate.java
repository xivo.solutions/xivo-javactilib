package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

/**
 * Created by jylebleu on 26/02/15.
 */
public class VoiceMailStatusUpdate  extends CtiResponseMessage<ConfigListener> {
    private long voiceMailId;
    private int newMessages = 0;
    private int waitingMessages = 0;
    private int oldMessages = 0;

    @Override
    public void notify(ConfigListener listener) {
        listener.onVoiceMailStatusUpdate(this);
    }

    public long getVoiceMailId() {
        return voiceMailId;
    }

    public void setVoiceMailId(long voiceMailId) {
        this.voiceMailId = voiceMailId;
    }

    public int getNewMessages() {
        return newMessages;
    }

    public void setNewMessages(int newMessages) {
        this.newMessages = newMessages;
    }

    public int getWaitingMessages() {
        return waitingMessages;
    }

    public void setWaitingMessages(int waitingMessages) {
        this.waitingMessages = waitingMessages;
    }

    public int getOldMessages() {
        return oldMessages;
    }

    public void setOldMessages(int oldMessages) {
        this.oldMessages = oldMessages;
    }
}
