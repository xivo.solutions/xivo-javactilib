package org.xivo.cti.message;

import java.util.ArrayList;
import java.util.List;

import org.xivo.cti.listener.IdsListener;

public class QueueMemberIds extends IdsList  {
    private final List<String> members;

    public QueueMemberIds() {
        members = new ArrayList<String>();
    }
    @Override
    public void notify(IdsListener listener) {
        listener.onQueueMemberIdsLoaded(this);
    }
    public List<String> getMemberIds() {
        return members;
    }

    public void add(String member) {
        members.add(member);
    }
}
