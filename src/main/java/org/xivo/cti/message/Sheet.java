package org.xivo.cti.message;

import java.util.Date;

import org.xivo.cti.listener.SheetListener;
import org.xivo.cti.model.Payload;

public class Sheet extends CtiResponseMessage<SheetListener> {

    public Date timenow = new Date();
    public boolean compressed;
    public String serial = "";
    public Payload payload = null;
    public String channel = "";

    @Override
    public String toString() {
        String s = "Sheet: ";
        s += ", serial: " + serial;
        s += ", channel: " + channel;
        s += ", timenow: " + timenow.toString();
        s += ", compressed: " + new Boolean(compressed).toString();
        if (payload != null)
            s += ", payload: " + payload.toString();
        else
            s += ", payload: null";
        return s;
    }

    @Override
    public void notify(SheetListener listener) {
        listener.onSheetReceived(this);
    }

}