package org.xivo.cti.message;

import org.xivo.cti.listener.ConfigListener;

public class QueueConfigUpdate extends ConfigUpdate {
    private String name = "";
    private String displayName = "";
    private String number = "";

    @Override
    public void notify(ConfigListener listener) {
        listener.onQueueConfigUpdate(this);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
