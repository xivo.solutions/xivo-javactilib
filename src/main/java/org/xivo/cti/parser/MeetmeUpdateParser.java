package org.xivo.cti.parser;

import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.message.CtiResponseMessage;
import org.xivo.cti.message.MeetmeUpdate;
import org.xivo.cti.model.Meetme;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MeetmeUpdateParser {

    private MeetmeMemberParser memberParser = new MeetmeMemberParser();

    public CtiResponseMessage<?> parse(JSONObject jsonObject) throws IllegalArgumentException {
        try {
            JSONObject jsonMeetmes = jsonObject.getJSONObject("config");
            List<Meetme> meetmes = new LinkedList<Meetme>();
            Iterator<String> it = jsonMeetmes.keys();
            while(it.hasNext()) {
                JSONObject meetme = jsonMeetmes.getJSONObject(it.next());
                meetmes.add(new Meetme(
                        meetme.getString("number"),
                        meetme.getString("name"),
                        BooleanParser.parse(meetme.getString("pin_required")),
                        DateParser.parse(meetme.getDouble("start_time")),
                        memberParser.parse(meetme.getJSONObject("members"))
                ));
            }
            return new MeetmeUpdate(meetmes);
        } catch (JSONException e) {
            throw new IllegalArgumentException("JSON parsing problem", e);
        }
    }
}
