package org.xivo.cti.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.message.AgentIds;
import org.xivo.cti.message.CtiResponseMessage;
import org.xivo.cti.message.IdsList;
import org.xivo.cti.message.PhoneIdsList;
import org.xivo.cti.message.QueueIds;
import org.xivo.cti.message.QueueMemberIds;
import org.xivo.cti.message.UserIdsList;
import org.xivo.cti.model.ObjectType;

public class ListParser {

    public CtiResponseMessage<?> parse(JSONObject idsList) throws JSONException {
        ObjectType objectType = ObjectType.valueOf(idsList.getString("listname").toUpperCase());
        switch(objectType) {
            case USERS:
                return parseUserIds(idsList);
            case PHONES:
                return parsePhoneIds(idsList);
            case QUEUES:
                return parseQueueIds(idsList);
            case AGENTS:
                return parseAgentIds(idsList);
            case QUEUEMEMBERS:
            return parseQueueMemberIds(idsList);
        }
        throw new JSONException(idsList.getString("listname") +" list not decoded : ");
    }


    public CtiResponseMessage<?> parseUserIds(JSONObject jsonUserIdsList) throws JSONException {
        UserIdsList usersIdsList = new UserIdsList();
        JSONArray jsonUserIds = jsonUserIdsList.getJSONArray("list");
        for (int i = 0; i < jsonUserIds.length(); i++) {
            usersIdsList.add(Integer.valueOf((String) jsonUserIds.get(i)));
        }
        return usersIdsList;
    }

    public CtiResponseMessage<?> parsePhoneIds(JSONObject idsList) throws JSONException {
        PhoneIdsList phoneIdsList = new PhoneIdsList();
        decodeIds(phoneIdsList,idsList);
        return phoneIdsList;
    }

    public CtiResponseMessage<?> parseQueueIds(JSONObject idsList) throws JSONException {
        QueueIds queueIds = new QueueIds();
        decodeIds(queueIds,idsList);
        return queueIds;
    }

    public CtiResponseMessage<?> parseAgentIds(JSONObject idsList) throws JSONException {
        AgentIds agentIds = new AgentIds();
        decodeIds(agentIds,idsList);
        return agentIds;
    }

    public CtiResponseMessage<?> parseQueueMemberIds(JSONObject idsList) throws JSONException {
        QueueMemberIds queueMemberIds = new QueueMemberIds();
        JSONArray jsonIds = idsList.getJSONArray("list");
        for (int i = 0; i < jsonIds.length(); i++) {
            queueMemberIds.add((String)jsonIds.get(i));
        }
        return queueMemberIds;
    }

    private void decodeIds(IdsList idsList, JSONObject jsonIdsList) throws NumberFormatException, JSONException {
        JSONArray jsonIds = jsonIdsList.getJSONArray("list");
        for (int i = 0; i < jsonIds.length(); i++) {
            idsList.add(Integer.valueOf((String) jsonIds.get(i)));
        }
    }
}
