package org.xivo.cti.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.MessageParser;
import org.xivo.cti.message.CtiResponseMessage;
import org.xivo.cti.message.QueueMemberAdded;
import org.xivo.cti.message.request.UsersAdded;
import org.xivo.cti.model.ObjectType;

import static org.xivo.cti.MessageParser.*;

public class AddedParser {

    public CtiResponseMessage<?> parse(JSONObject jsonAddedInConfig) throws JSONException {
        String listName = jsonAddedInConfig.getString("listname");
        ObjectType objectType = ObjectType.valueOf(listName.toUpperCase());
        switch (objectType) {
            case USERS:
                return parseUserAdded(jsonAddedInConfig);
            case QUEUEMEMBERS:
                return parseQueueMemberAdded(jsonAddedInConfig);
            default:
                break;
        }
        return null;
    }
    private CtiResponseMessage<?> parseUserAdded(JSONObject usersAddedMsg) throws JSONException {
        UsersAdded usersAdded = new UsersAdded();
        JSONArray jsonUserIds = usersAddedMsg.getJSONArray("list");
        for (int i = 0; i < jsonUserIds.length(); i++) {
            usersAdded.add(Integer.valueOf((String) jsonUserIds.get(i)));
        }

        return usersAdded;
    }
    private CtiResponseMessage<?> parseQueueMemberAdded(JSONObject queueMemberAddedMsg) throws JSONException {
        QueueMemberAdded qmAdded = new QueueMemberAdded();
        JSONArray members = queueMemberAddedMsg.getJSONArray("list");
        for (int i = 0; i < members.length(); i++) {
            qmAdded.setAgentNumber(getAgentNumberFromMember(members.get(i).toString()));
            qmAdded.setQueueName(getQueueNameFromMember(members.get(i).toString()));
        }

        return qmAdded;
    }
}
