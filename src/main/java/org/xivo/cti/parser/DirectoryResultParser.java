package org.xivo.cti.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.message.DirectoryResult;
import org.xivo.cti.model.DirectoryEntry;

public class DirectoryResultParser {

    public DirectoryResult parse(JSONObject jsonDirectoryResult) throws JSONException {
        DirectoryResult directoryResult = new DirectoryResult();
        JSONArray jsonHeaders = jsonDirectoryResult.getJSONArray("headers");
        for (int i = 0; i < jsonHeaders.length(); i ++) {
            directoryResult.addHeader(jsonHeaders.get(i).toString());
        }

        JSONArray jsonEntries = jsonDirectoryResult.getJSONArray("resultlist");
        for (int i = 0; i < jsonEntries.length(); i ++) {
            String entryToParse = jsonEntries.get(i).toString();
            DirectoryEntry entry = new DirectoryEntry();
            String[] fields = entryToParse.split(";");
            for (int j = 0; j < fields.length; j++) {
                entry.addField(fields[j]);
            }
            directoryResult.addEntry(entry);
        }
        return directoryResult;
    }

}
