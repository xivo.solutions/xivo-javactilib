package org.xivo.cti.parser;

import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.message.ChannelStatus;

public class ChannelStatusParser {

    public ChannelStatus parseStatusUpdate(JSONObject jsonChannelStatusUpdate) throws JSONException {
        ChannelStatus channelStatus = new ChannelStatus();
        channelStatus.channelId = jsonChannelStatusUpdate.getString("tid");

        JSONObject jsonChannelStatus = jsonChannelStatusUpdate.getJSONObject("status");
        channelStatus.timeStamp = jsonChannelStatus.getDouble("timestamp");
        channelStatus.holded = jsonChannelStatus.getBoolean("holded");
        channelStatus.commstatus = jsonChannelStatus.getString("commstatus");
        channelStatus.parked = jsonChannelStatus.getBoolean("parked");
        channelStatus.state = jsonChannelStatus.getString("state");
        channelStatus.direction = jsonChannelStatus.optString("direction");
        channelStatus.talkingto_kind = jsonChannelStatus.optString("talkingto_kind");
        channelStatus.talkingto_id = jsonChannelStatus.optString("talkingto_id");
        return channelStatus;
    }

}
