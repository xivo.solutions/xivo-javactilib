package org.xivo.cti.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.message.CtiResponseMessage;
import org.xivo.cti.message.LoginCapasAck;
import org.xivo.cti.model.Action;
import org.xivo.cti.model.Capacities;
import org.xivo.cti.model.PhoneStatus;
import org.xivo.cti.model.Service;
import org.xivo.cti.model.UserStatus;
import org.xivo.cti.model.XiVOPreference;
import org.xivo.cti.model.Xlet;

public class LoginCapasAckParser {

    public CtiResponseMessage<?> parse(JSONObject loginCapasAckJson) throws JSONException {
        LoginCapasAck loginCapasAck = new LoginCapasAck();
        loginCapasAck.presence = loginCapasAckJson.optString("presence");
        loginCapasAck.userId = loginCapasAckJson.getString("userid");
        loginCapasAck.applicationName = loginCapasAckJson.getString("appliname");
        loginCapasAck.capacities = parseCapacities(loginCapasAckJson.getJSONObject("capas"));
        loginCapasAck.xlets = parseXlets(loginCapasAckJson.getJSONArray("capaxlets"));
        return loginCapasAck;
    }


    protected Capacities parseCapacities(JSONObject capacitiesJson) throws JSONException {
        Capacities capacities = new Capacities();
        try {
            capacities.setPreferences(parsePreferences(capacitiesJson.getJSONObject("preferences")));
        } catch (JSONException e) {
        }
        capacities.setUsersStatuses(parseUserStatuses(capacitiesJson.getJSONObject("userstatus")));
        capacities.setServices(parseServices(capacitiesJson.getJSONArray("services")));
        capacities.setPhoneStatuses(parsePhoneStatuses(capacitiesJson.getJSONObject("phonestatus")));
        return capacities;

    }

    protected List<Xlet> parseXlets(JSONArray xletsJson) throws JSONException {
        List<Xlet> xlets = new ArrayList<Xlet>();

        for (int i = 0; i < xletsJson.length(); i++) {
            String order = null;
            if (xletsJson.getJSONArray(i).length() > 2) {
                order = xletsJson.getJSONArray(i).getString(2);
            }
            Xlet xlet = new Xlet(xletsJson.getJSONArray(i).getString(0), xletsJson.getJSONArray(i).getString(1), order);
            xlets.add(xlet);
        }
        return xlets;
    }

    protected List<XiVOPreference> parsePreferences(JSONObject jsonPreferences) throws JSONException {
        List<XiVOPreference> xiVOPreferences = new ArrayList<XiVOPreference>();
        @SuppressWarnings("unchecked")
        Iterator<String> keys = jsonPreferences.keys();
        while (keys.hasNext()) {
            String parameter = keys.next();
            XiVOPreference xiVOPreference = new XiVOPreference(parameter, jsonPreferences.getString(parameter));
            xiVOPreferences.add(xiVOPreference);
        }
        return xiVOPreferences;
    }

    public List<UserStatus> parseUserStatuses(JSONObject userStatusesJson) throws JSONException {
        List<UserStatus> userStatuses = new ArrayList<UserStatus>();
        @SuppressWarnings("unchecked")
        Iterator<String> keys = userStatusesJson.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            userStatuses.add(parseUserStatus(key, userStatusesJson.getJSONObject(key)));
        }
        return userStatuses;
    }


    public List<Service> parseServices(JSONArray servicesJson) throws JSONException {
        List<Service> services = new ArrayList<Service>();

        for (int i = 0; i < servicesJson.length(); i++) {
            services.add(new Service(servicesJson.get(i).toString()));
        }

        return services;
    }


    protected List<PhoneStatus> parsePhoneStatuses(JSONObject phoneStatusesJson) throws JSONException {
        List<PhoneStatus> phoneStatuses = new ArrayList<PhoneStatus>();
        @SuppressWarnings("unchecked")
        Iterator<String> keys = phoneStatusesJson.keys();
        while (keys.hasNext()) {
            String statusId = keys.next();
            JSONObject phoneStatusJson = phoneStatusesJson.getJSONObject(statusId);
            PhoneStatus phoneStatus = new PhoneStatus(statusId, phoneStatusJson.getString("color"),
                    phoneStatusJson.getString("longname"));
            phoneStatuses.add(phoneStatus);
        }
        return phoneStatuses;
    }


    public UserStatus parseUserStatus(String key, JSONObject userStatusJson) throws JSONException {
        UserStatus status = new UserStatus(key);
        status.setColor(userStatusJson.getString("color"));
        status.setLongName(userStatusJson.getString("longname"));
        if (userStatusJson.has("allowed")) {
            JSONArray allowedJson = userStatusJson.getJSONArray("allowed");
            for (int i = 0; i < allowedJson.length(); i++) {
                status.allow(allowedJson.get(i).toString());
            }
        }
        if (userStatusJson.has("actions")) {
            JSONObject actionsJson = userStatusJson.getJSONObject("actions");
            @SuppressWarnings("unchecked")
            Iterator<String> actionNames = actionsJson.keys();
            while (actionNames.hasNext()) {
                String name = actionNames.next();
                Action action = new Action(name, actionsJson.getString(name));
                status.addAction(action);
            }
        }
        return status;
    }

}
