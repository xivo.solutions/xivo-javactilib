package org.xivo.cti.parser;

import org.json.JSONObject;
import org.xivo.cti.model.MeetmeMember;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class MeetmeMemberParser {


    public List<MeetmeMember> parse(JSONObject members) {
        try {
            List<MeetmeMember> memberList = new LinkedList<MeetmeMember>();
            Iterator<String> it = members.keys();
            while(it.hasNext()) {
                JSONObject member = members.getJSONObject(it.next());
                int joinOrder = member.getInt("join_order");
                Date joinTime = DateParser.parse(member.getDouble("join_time"));
                String number = member.getString("number");
                String name = member.getString("name");
                boolean muted = BooleanParser.parse(member.getString("muted"));
                memberList.add(new MeetmeMember(joinOrder, joinTime, muted, name, number));
            }
            return memberList;
        } catch(Exception e) {
            throw new IllegalArgumentException("JSON parsing problem", e);
        }
    }
}
