package org.xivo.cti.parser;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.JSONException;
import org.json.JSONObject;
import org.xivo.cti.message.QueueStatistics;
import org.xivo.cti.model.Counter;
import org.xivo.cti.model.StatName;

public class StatParser {
    private static String PREFIX = "Xivo-";
    private static String REMOVE = "-";

    private Logger logger = Logger.getLogger(getClass().getName());

    public QueueStatistics parse(JSONObject jsonObject) throws JSONException {
        QueueStatistics queueStatistics = new QueueStatistics();
        JSONObject stats = jsonObject.getJSONObject("stats");
        String queueId = stats.keys().next().toString();
        queueStatistics.setQueueId(Integer.valueOf(queueId));
        JSONObject counters = stats.getJSONObject(queueId);

        @SuppressWarnings("unchecked")
        Iterator<String> counterNames = counters.keys();
        while (counterNames.hasNext()) {
            String counterName = counterNames.next();
            try {
                if (counters.get(counterName).toString().length() > 0) {
                    Counter counter = new Counter(
                            StatName.valueOf(counterName.replace(PREFIX, "").replace(REMOVE, "")),
                            counters.getInt(counterName));
                    queueStatistics.addCounter(counter);
                }
            } catch (IllegalArgumentException e) {
                logger.log(Level.WARNING, "Could not parse the message " + jsonObject.toString(), e);
            }
        }
        return queueStatistics;
    }
}
